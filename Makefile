all: usbnotifier 

usbnotifier:  main.c
	gcc -o $@ $^ `pkg-config libusb-1.0 --cflags --libs` 

install: usbnotifier
	cp ./usbnotifier /usr/local/bin
